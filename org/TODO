To Do List of the Security Team
===============================
Items which are not related to preparing security updates, e.g. work on
infrastructure

Category
 - task (who is on it)

--BEGIN
Infrastructure
 - remove all reference to Security Audit
   https://www.debian.org/security/audit/
 - svnsync setup on soler to back up alioth in near-realtime (fw)
 - sec-private Subversion or Git repository on seger (fw, carnil)
   - check for using git-remote-gcrypt (carnil)
   - notify DSA and verify it is part of the backup
 - Disable RT queues for Security; clarify with DSA if a 'autoresponder
   not including the mailtext can be activated for a transitional period
   to redirect to request to be resent to the team alias
 - Clarify with ftp-masters status of unembargoed and embargoed queues
   on security-master
 - Plan for renaming alioth project from secure-testing ->
   security-tracker. Contact alioth admins.

Security Tracker
 - ask Jon Wiltshire if new status to differentiate between "no-dsa, if
   the maintainer wants to fix in a point update go ahead" and "no-dsa,
   was ignored because it's possible to backport" is still needed. (fw)

Security Tracker svn to git conversion
 - svn author list generation and conversion of svn repository to git
   repository:
   * Guide: http://git-scm.com/book/en/Git-and-Other-Systems-Migrating-to-Git
 - joeyh's commit script needs to be adopted to git
   * When fixing the joeyh one, I think it makes sense to move it to a
     role account on alioth (as previously discussed), rather than this
     personal account, at the same time.
 - External check cronjob from Raphael
   * When fixing it, also migrate to the role account
 - Daily DSA status report to team alias
   * Should also move to role account
 - the tracker itself needs to be adopted
 - Checkout on moszumanska in /home/groups/secure-testing (See
   README.repo there)
 - There's also a very useful pre-commit hook that checks syntax of
   commits to data/*. This is something that also would need a place
   somewhere/in the git repository.
 - the sectracker user is subscribed to the commits mailinglists, and
   the commit messages trigger updates of the tracker.
 - http://security-team.debian.org (on dillon.debian.org) is updated from svn,
   needs to be switched (simple)
 - https://contributors.debian.org/source/Debian%20Security%20Tracker
 - Allocating DSA's + DLA's: svn guarantees we do not race on DSA+DLA
   allocations via DSA/DLA files. Having distributed VCS we would need
   to avoid races on DSA+DLA allocations.

Organisation
 - Compile a list of packages for which helpers with test setups are
   wanted (jmm)


Web pages
 - rename "Mitre CVE database" to "CVE IDs" (fw)
 - replace CVE cross-reference with links to approrate security tracker
   information
 - check if the developers-reference (https://www.debian.org/doc/manuals/developers-reference/pkgs.html#bug-security)
   still holds updated information.
 - check if the security related information in wiki.debian.org is updated. (luciano)
   - Teams/TestingSecurity (tagged as deprecated)
   - http://testing-security.debian.net/
   - https://www.debian.org/doc/manuals/securing-debian-howto/ch10.en.html#s-security-support-testing
 - Create webpage like release team has (http://release.debian.org) e.g. pointing
   to http://security-team.debian.org holding all relevant entry points for tasks,
   relevant information on workflows, etc ... (luciano)
