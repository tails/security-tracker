A wheezy-lts security update is needed for the following source packages.
When you add a new entry, please keep the list alphabetically sorted.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

To pick an issue, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

--
ca-certificates
  NOTE: 20170719: maintainer will handle the upload, see https://lists.debian.org/d0b9674a-ac5b-5cc9-1982-fb6f36155c5a@pbandjelly.org
  NOTE: 20171013: pinged maintainer: https://lists.debian.org/87efpuc95w.fsf@curie.anarc.at (anarcat)
--
couchdb
  NOTE: Only in wheezy, we are on our own.
--
graphicsmagick (Markus Koschany)
--
icu
  NOTE: 20171229: CVE-2017-15422 was reported via Google Code issue report in Chromium project; report is not visible to the public
--
imagemagick (Markus Koschany)
--
lame (Hugo Lefeuvre)
  NOTE: Couldn't reproduce CVE-2017-{69-72}, but successfully reproduced CVE-2017-150{18,45,46}
  NOTE: 20171120: Backporting 3.100 is not conceivable, diff >40k lines.
  NOTE: Instead, lame's maintainer will switch jessie to also use libsndfile in the next Jessie
  NOTE: point update, simply forward the changes to Wheezy (this should fix almost all open CVEs).
--
libav (Hugo Lefeuvre)
  NOTE: 20171116: Diego Biurrun (from the libav team) is working on patches.
--
libreoffice (Emilio Pozuelo)
  NOTE: regression update, see:
  NOTE: https://lists.debian.org/debian-lts/2017/05/msg00012.html
--
libvorbis (Guido Günther)
  NOTE: Underlying reason for CVE-2017-14160 yet unclear, no ustream feedback on this issue.
  NOTE: Fixes for other CVEs applied upstream and in sid.
--
linux
--
ming (Hugo Lefeuvre)
  NOTE: 20171120: wip, currently working on it with upstream, might take a while
  NOTE: Some issues currently in upstream's bug tracker are missing a CVE number, so number of issues might increase in the next weeks
--
mupdf
  NOTE: 20171224: Upstream patch does not apply to LTS cleanly. Might need hanges to apps/pdfclean.c rather than pdf-write.c (lamby)
--
swftools (Guido Günther)
  NOTE: 20171118: At least CVE-2017-16797 is present. (lamby)
  NOTE: 20171210: likely to be turned into a pkg with limited sec support
--
tiff (Roberto C. Sánchez)
--
tiff3 (Roberto C. Sánchez)
--
wordpress
  NOTE: 2017-12-25: Fix requires migrating users from MD5 -> bcrypt. (lamby)
--
xen
--
