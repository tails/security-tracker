PYTHON = python
PYTHON_MODULES = $(wildcard lib/python/*.py)
BUG_LISTS = $(wildcard data/*/list)

# Adjust these if necessary.  The architecture selection is rather
# arbitrary at the moment.  More architectures can be added later.

MIRROR = http://debian.csail.mit.edu/debian/
wheezy_ARCHS = amd64
jessie_ARCHS = amd64
stretch_ARCHS = amd64
buster_ARCHS = amd64
sid_ARCHS = amd64

OLDOLDSTABLE = wheezy
OLDSTABLE    = jessie
STABLE       = stretch
TESTING      = buster

TAILS_GITWEB                 = https://git-tails.immerda.ch/tails
TAILS_MIRROR_BASE_URL        = http://time-based.snapshots.deb.tails.boum.org
TAILS_MIRROR_DEBIAN          = $(TAILS_MIRROR_BASE_URL)/debian/
TAILS_MIRROR_DEBIAN_SECURITY = $(TAILS_MIRROR_BASE_URL)/debian-security/
TAILS_ARCHS = amd64
TAILS_GIT_SNAPSHOTS_DOT_D    = $(TAILS_GITWEB)/plain/config/APT_snapshots.d

TAILS_NIGHTLY_BASE_URL          = https://nightly.tails.boum.org
TAILS_STABLE_BUILD_MANIFEST_URL = $(TAILS_NIGHTLY_BASE_URL)/build_Tails_ISO_feature-buster/lastSuccessful/archive/latest.iso.build-manifest

# Let's assume that:
# 1. stable and testing are always frozen
# 2. devel is never frozen
# 3. all branches use the "latest" snapshot for debian-security
TAILS_STABLE_DEBIAN_SERIAL   = $(shell curl -s $(TAILS_GIT_SNAPSHOTS_DOT_D)/debian/serial?h=stable)
TAILS_TESTING_DEBIAN_SERIAL  = $(shell curl -s $(TAILS_GIT_SNAPSHOTS_DOT_D)/debian/serial?h=testing)
TAILS_DEVEL_DEBIAN_SERIAL    = $(shell curl -s $(TAILS_MIRROR_DEBIAN)/project/trace/debian | awk -F': ' '/^Archive serial: / {print $$2}')
TAILS_DEBIAN_SECURITY_SERIAL = $(shell curl -s $(TAILS_MIRROR_DEBIAN_SECURITY)/project/trace/debian-security | awk -F': ' '/^Archive serial: / {print $$2}')

all:
	$(PYTHON) bin/update-db data/security.db

clean:
	-rm -f data/security.db lib/python/test_security.db
	-rm -f stamps/*-*

.PHONY: check check-syntax

test check: check-syntax

check-syntax: stamps/CVE-syntax \
	stamps/DSA-syntax stamps/DTSA-syntax stamps/DLA-syntax

stamps/CVE-syntax: data/CVE/list bin/check-syntax $(PYTHON_MODULES)
	$(PYTHON) bin/check-syntax CVE data/CVE/list
	touch $@

stamps/DSA-syntax: data/DSA/list bin/check-syntax $(PYTHON_MODULES)
	$(PYTHON) bin/check-syntax DSA data/DSA/list
	touch $@

stamps/DTSA-syntax: data/DTSA/list bin/check-syntax $(PYTHON_MODULES)
	$(PYTHON) bin/check-syntax DTSA data/DTSA/list
	touch $@

stamps/DLA-syntax: data/DLA/list bin/check-syntax $(PYTHON_MODULES)
	$(PYTHON) bin/check-syntax DLA data/DLA/list
	touch $@

.PHONY: serve
serve:
	@bash bin/test-web-server

.PHONY: update-packages update-testing update-stable update-oldstable update-oldoldstable update-tails update-tails-stable-debian update-tails-testing-debian update-tails-devel-debian update-tails-debian-security
update-packages: update-testing
	set -e ; for rel in sid ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(MIRROR)/dists/$$rel/$$archive/source/Sources \
			data/packages/$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $(sid_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(MIRROR)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-testing:
	set -e ; for rel in $(TESTING) ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(MIRROR)/dists/$$rel/$$archive/source/Sources \
			data/packages/$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $($(TESTING)_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(MIRROR)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-stable:
	set -e ; for rel in $(STABLE) ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(MIRROR)/dists/$$rel/$$archive/source/Sources \
			data/packages/$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $($(STABLE)_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(MIRROR)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-oldstable:
	set -e ; for rel in $(OLDSTABLE) ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(MIRROR)/dists/$$rel/$$archive/source/Sources \
			data/packages/$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $($(OLDSTABLE)_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(MIRROR)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-oldoldstable:
	set -e ; for rel in $(OLDOLDSTABLE) ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(MIRROR)/dists/$$rel/$$archive/source/Sources \
			data/packages/$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $($(OLDOLDSTABLE)_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(MIRROR)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-tails-stable-debian:
	set -e ; for rel in buster ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(TAILS_MIRROR_DEBIAN)/$(TAILS_STABLE_DEBIAN_SERIAL)/dists/$$rel/$$archive/source/Sources \
			data/packages/tails-stable-$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $(TAILS_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(TAILS_MIRROR_DEBIAN)/$(TAILS_STABLE_DEBIAN_SERIAL)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/tails-stable-$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-tails-testing-debian:
	set -e ; for rel in buster ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(TAILS_MIRROR_DEBIAN)/$(TAILS_TESTING_DEBIAN_SERIAL)/dists/$$rel/$$archive/source/Sources \
			data/packages/tails-testing-$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $(TAILS_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(TAILS_MIRROR_DEBIAN)/$(TAILS_TESTING_DEBIAN_SERIAL)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/tails-testing-$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

update-tails-devel-debian:
	set -e ; for rel in buster ; do \
		for archive in main contrib non-free ; do \
		$(PYTHON) bin/apt-update-file \
			$(TAILS_MIRROR_DEBIAN)/$(TAILS_DEVEL_DEBIAN_SERIAL)/dists/$$rel/$$archive/source/Sources \
			data/packages/tails-devel-$${rel}__$${archive}_Sources ; \
		done ; \
	        for arch in $(TAILS_ARCHS) ; do \
		  for archive in main contrib non-free ; do \
		  $(PYTHON) bin/apt-update-file \
		    $(TAILS_MIRROR_DEBIAN)/$(TAILS_DEVEL_DEBIAN_SERIAL)/dists/$$rel/$$archive/binary-$$arch/Packages \
		    data/packages/tails-devel-$${rel}__$${archive}_$${arch}_Packages ; \
		  done ; \
		done ; \
	done

.PHONY: update-security update-old-security update-oldold-security update-testing-security
ST_MIRROR = http://security.debian.org/dists/$(TESTING)/updates
ST_FILE = data/packages/$(TESTING)_security_
update-testing-security:
	for section in main contrib non-free ; do \
	  $(PYTHON) bin/apt-update-file \
	    $(ST_MIRROR)/$$section/source/Sources $(ST_FILE)$${section}_Sources ; \
	  set -e ; for arch in $($(TESTING)_ARCHS) ; do \
	    $(PYTHON) bin/apt-update-file \
	      $(ST_MIRROR)/$$section/binary-$${arch}/Packages $(ST_FILE)$${section}_$${arch}_Packages ; \
	  done ; \
	done

SEC_MIRROR = http://security.debian.org/dists
update-security:
	for archive in $(STABLE); do \
            for section in main contrib non-free ; do \
	    $(PYTHON) bin/apt-update-file \
	      $(SEC_MIRROR)/$$archive/updates/$$section/source/Sources \
	      data/packages/$${archive}_security_$${section}_Sources ; \
	    for arch in $($(STABLE)_ARCHS) ; do \
	      $(PYTHON) bin/apt-update-file \
	        $(SEC_MIRROR)/$$archive/updates/$$section/binary-$$arch/Packages \
	        data/packages/$${archive}_security_$${section}_$${arch}_Packages ; \
	    done ; \
	  done ; \
	done

update-old-security:
	for archive in $(OLDSTABLE); do \
            for section in main contrib non-free ; do \
	    $(PYTHON) bin/apt-update-file \
	      $(SEC_MIRROR)/$$archive/updates/$$section/source/Sources \
	      data/packages/$${archive}_security_$${section}_Sources ; \
	    for arch in $($(OLDSTABLE)_ARCHS) ; do \
	      $(PYTHON) bin/apt-update-file \
	        $(SEC_MIRROR)/$$archive/updates/$$section/binary-$$arch/Packages \
	        data/packages/$${archive}_security_$${section}_$${arch}_Packages ; \
	    done ; \
	  done ; \
	done

update-oldold-security:
	for archive in $(OLDOLDSTABLE); do \
            for section in main contrib non-free ; do \
	    $(PYTHON) bin/apt-update-file \
	      $(SEC_MIRROR)/$$archive/updates/$$section/source/Sources \
	      data/packages/$${archive}_security_$${section}_Sources ; \
	    for arch in $($(OLDOLDSTABLE)_ARCHS) ; do \
	      $(PYTHON) bin/apt-update-file \
	        $(SEC_MIRROR)/$$archive/updates/$$section/binary-$$arch/Packages \
	        data/packages/$${archive}_security_$${section}_$${arch}_Packages ; \
	    done ; \
	  done ; \
	done

update-backports: update-backports-$(STABLE)

update-backports-%:
	set -e && archive=$(shell echo $@ | cut -d- -f3) ; \
          for section in main contrib non-free ; do \
	    for arch in $($(shell echo $@ | cut -d- -f3)_ARCHS) ; do \
	      $(PYTHON) bin/apt-update-file \
	        $(MIRROR)/dists/$${archive}-backports/$$section/binary-$$arch/Packages \
	        data/packages/$${archive}-backports__main_$${arch}_Packages ; \
	    done ; \
	    $(PYTHON) bin/apt-update-file \
	      $(MIRROR)/dists/$${archive}-backports/$$section/source/Sources \
	      data/packages/$${archive}-backports__main_Sources ; \
	  done ; \

update-lists:
	git fetch -q origin && git checkout -f origin/master -- data

# Since October 16, 2015 the XML data feeds are no longer available for
# download in an uncompressed format.
update-nvd:
	mkdir -p data/nvd
	for x in $$(seq 2002 $$(date +%Y)) ; do \
	  name=nvdcve-$$x.xml.gz; \
	  wget --directory-prefix=data/nvd --timestamping -q https://nvd.nist.gov/download/$$name || true; \
	  gzip --keep -f -d data/nvd/$$name || true; \
	done
	python bin/update-nvd data/nvd/nvdcve-*.xml

# Experimental code to compare the Debian and NVD CVE databases using
# CPE values as common key.
update-compare-nvd:
	mkdir -p data/nvd2
	for x in $$(seq 2002 $$(date +%Y)) ; do \
	  name=nvdcve-2.0-$$x.xml.gz; \
	  wget --directory-prefix=data/nvd2 --timestamping -q https://static.nvd.nist.gov/feeds/xml/cve/$$name || true ; \
	  gzip --keep -f -d data/nvd2/$$name || true; \
	done
	bin/compare-nvd-cve 2> compare-nvd-cve.log

update-tails-build-manifests:
	mkdir -p data/Tails/build-manifests/stable
	wget --directory-prefix=data/Tails/build-manifests/stable \
	     --timestamping \
	     -q \
	     $(TAILS_STABLE_BUILD_MANIFEST_URL)

update-tails: update-tails-build-manifests update-tails-stable-debian update-tails-testing-debian update-tails-devel-debian update-tails-debian-security

debug-tails-snapshots:
	@echo stable/debian:   $(TAILS_STABLE_DEBIAN_SERIAL)
	@echo testing/debian:  $(TAILS_TESTING_DEBIAN_SERIAL)
	@echo devel/debian:    $(TAILS_DEVEL_DEBIAN_SERIAL)
	@echo debian-security: $(TAILS_DEBIAN_SECURITY_SERIAL)

update-all: update-nvd update-lists update-packages update-stable update-security update-testing-security update-packages update-backports update-tails all
